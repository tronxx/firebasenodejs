const { Router } = require('express');
const { render } = require('../app');
const admin = require('firebase-admin');
const fs = require('firebase-admin');

const serviceAccount = require('./key.json');

fs.initializeApp({
  credential: fs.credential.cert(serviceAccount)
 });

const db = fs.firestore(); 

const router = Router();

router.get( '/', (req, res) => {
    res.render('index')    
}
);

router.post('/nuevocontacto', (req, res) => {
  console.log(req.body);
  const contactos = db.collection('contactos');
  const liam = contactos.doc('newcontacto');
  liam.set(req.body); 
  res.send('recibido');
});

module.exports = router;